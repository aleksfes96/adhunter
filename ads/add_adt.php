<?php

    session_start();

    include("../db/settings.php");
    include("../auth/common.php");

    if(!is_admin()) {
        ?>
        <div class="alert alert-danger col-sm-2" role="alert">Необходимо авторизироваться</div>
        <a class="btn btn-danger" href="/adhunter/auth/sign_in.php">Войти</a>
        <?php
        exit;
    }

    $link = db_connection();

    if(isset($_POST['add-ad']) && $_POST['blocker-add-ad'] != $_SESSION['current-query-add-ad']) {

        $id = create_id($link);
        if (!mysqli_query(
                $link,
        "INSERT INTO ads (
                    id, 
                    network, 
                    type, 
                    genre, 
                    name, 
                    description, 
                    price, 
                    followers_count, 
                    followers_age, 
                    visits_count)
                VALUES (
                    $id, 
                    '{$_POST['network']}', 
                    '{$_POST['type']}', 
                    '{$_POST['genre']}', 
                    '{$_POST['name']}', 
                    '{$_POST['description']}', 
                    {$_POST['price']}, 
                    {$_POST['followers_count']}, 
                    {$_POST['followers_age']}, 
                    {$_POST['visits_count']});"
            )
        )
            printf("Errormessage: %s\n", mysqli_error($link));

        $_SESSION['current-query-add-ad'] = $_POST['blocker-add-ad'];
    }

    header("Location: " . $_SERVER['HTTP_REFERER']);
    exit;

    function create_id($link) {

        do {

            $id = rand(1,1000);
            $res = mysqli_query($link,"SELECT id FROM ads WHERE id=$id;");

        } while(mysqli_num_rows($res));

        return $id;
    }
