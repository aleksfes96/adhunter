<?php
include("../db/settings.php");
include("../auth/common.php");
include("../decor/common.php");

session_start();
?>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Реклама</title>
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="../css/style.css" rel="stylesheet">
    </head>

    <body>
    <?php

    if(!is_admin()) {
        ?>
        <div class="alert alert-danger col-sm-2" role="alert">Необходимо авторизироваться</div>
        <a class="btn btn-danger" href="/adhunter/auth/sign_in.php">Войти</a>
        <?php
        exit;
    }

    $link = db_connection();
    _header();
    menu();
    ?>
    <div class="content">

        <h1>
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            Реклама:
        </h1>
        <br>
        <hr>
        <?php ads_table($link); ?>

        <form id="form-add-ad" action="add_adt.php" method="post">
            <input name="blocker-add-ad" type="hidden" value="query-add-ad-<?php echo rand(0,999999);?>">
        </form>

    </div>
    <script src="../bootstrap/js/bootstrap.js"></script>
    </body>
    </html>

<?php
function ads_table($link) {

    ?>
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>id</th>
            <th>Соц.сеть</th>
            <th>Тип</th>
            <th>Название</th>
            <th>Жанр</th>
            <th>Описание</th>
            <th>Число подписчиков</th>
            <th>Средний возраст подписчиков</th>
            <th>Средняя посещаемость</th>
            <th>Стоимость</th>
            <th>Контакт</th>
            <th>

            </th>
        </tr>
        </thead>
        <tbody>
        <?php ad_row_input(); ?>
        <?php ads_rows($link); ?>
        </tbody>
    </table>
    <?php
}
function ads_rows($link) {
    $res = mysqli_query($link,"SELECT * FROM ads;");
    foreach ($res as $ad) {
        ?>
            <tr>
                <td><?php echo $ad['id'];?></td>
                <td><?php echo $ad['network'];?></td>
                <td><?php echo $ad['type'];?></td>
                <td><?php echo $ad['name'];?></td>
                <td><?php echo $ad['genre'];?></td>
                <td><?php echo $ad['description'];?></td>
                <td><?php echo $ad['followers_count'];?></td>
                <td><?php echo $ad['followers_age'];?></td>
                <td><?php echo $ad['visits_count'];?></td>
                <td><?php echo $ad['contact'];?></td>
                <td><?php echo $ad['price'];?></td>
                <td></td>
            </tr>
            <?php
    }
    unset($ad);
}
function ad_row_input() {
    ?>
    <tr>
        <td class="icon">
            <i class="fa fa-plus" style="font-size:16px"></i>
        </td>
        <td>
            <select required name="network" form="form-add-ad" class="form-control">
                <option value="vk">ВК</option>
                <option value="ok">Одноклассники</option>
                <option value="inst">Инстаграм</option>
            </select>
        </td>
        <td>
            <select required name="type" form="form-add-ad" class="form-control">
                <option value="post">Пост/Запись</option>
                <option value="clip">Закрепленный пост/запись</option>
                <option value="story">История</option>
            </select>
        </td>
        <td>
            <input name="name" required maxlength="100" placeholder="Введите название" class="form-control" 
                   form="form-add-ad"/>
        </td>
        <td>
            <select required name="genre" form="form-add-ad" class="form-control">
                <option value="rock">Рок</option>
                <option value="rap">Рэп</option>
                <option value="pop">Поп</option>
                <option value="electronic">Электро</option>
                <option value="classical">Классика</option>
                <option value="jazz">Джаз</option>
            </select>
        </td>
        <td>
            <input name="description" required maxlength="255" placeholder="Введите описание" class="form-control" 
                   form="form-add-ad"/>
        </td>
        <td>
            <input name="followers_count" required type="number" placeholder="Число подписчиков" min="0" step="1"
                   class="form-control" form="form-add-ad"/>
        </td>
        <td>
            <input name="followers_age" required type="number" placeholder="Средний возраст подписчиков" min="0" step="1"
                   class="form-control" form="form-add-ad"/>
        </td>
        <td>
            <input name="visits_count" required type="number" placeholder="Средняя посещаемость" min="0" step="1"
                   class="form-control" form="form-add-ad"/>
        </td>
        <td>
            <input name="price" required type="number" placeholder="Стоимость" step="0.01" min="0"
                   class="form-control" form="form-add-ad"/>
        </td>
        <td>
            <input name="contact" required maxlength="100" placeholder="Контактные данные" class="form-control"
                   form="form-add-ad"/>
        </td>
        <td>
            <input type="submit" value="Добавить" name="add-ad" class="btn btn-danger col-sm-12" form="form-add-ad"/>
        </td>
    </tr>
    <?php
}
