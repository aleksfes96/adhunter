<?php
include("db/settings.php");
include("auth/common.php");
include("decor/common.php");

session_start();
?>
<html>
<head>
    <meta charset="utf-8">
    <title>AD HUNTER - музыкальный охотник за рекламой</title>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
<?php

    $link = db_connection();
    _header();
    menu();
?>
<div class="content">

    <br>
    <br>
    <p style="text-align: center; font-size: large;">
        AD HUNTER - ресурс для поиска в социальных сетях рекламных площадок, направленных на музыкальную область
    </p>
    <br>

    <div class="container">
        <div class="row">
            <a class='btn btn-default btn-lg col-sm-push-5 col-sm-2 ' href='search/'>Начать поиск</a>
        </div>
    </div>

</div>
<script src="js/bootstrap.js"></script>
</body>
</html>