<?php
include("../db/settings.php");
include("../decor/common.php");

session_start();
?>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Поиск рекламы</title>
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="../css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>

    <body>

    <?php

    $link = db_connection();

    _header();
    menu();
    ?>
    <div class="content">

        <h2>
            <span class="glyphicon glyphicon-search"></span>
            Поиск:
        </h2>

        Соц.сеть
        <select required name="network" form="form-ad-search" class="form-control">
            <option value="all" <?php selected('network', 'all');?>>
                Неважно
            </option>
            <option value="vk" <?php selected('network', 'vk');?>>
                ВК
            </option>
            <option value="ok" <?php selected('network', 'ok');?>>
                Одноклассники
            </option>
            <option value="inst" <?php selected('network', 'inst');?>>
                Инстаграм
            </option>
        </select>

        Тип рекламы
        <select required name="type" form="form-ad-search" class="form-control">
            <option value="all" <?php selected('type', 'all');?>>
                Неважно
            </option>
            <option value="post" <?php selected('type', 'post');?>>
                Пост/Запись
            </option>
            <option value="clip" <?php selected('type', 'clip');?>>
                Закрепленный пост/запись
            </option>
            <option value="story" <?php selected('type', 'story');?>>
                История
            </option>
        </select>

        Жанр
        <select required name="genre" form="form-ad-search" class="form-control">
            <option value="all" <?php selected('genre', 'all');?>>
                Неважно
            </option>
            <option value="rock" <?php selected('genre', 'rock');?>>
                Рок
            </option>
            <option value="rap" <?php selected('genre', 'rap');?>>
                Рэп
            </option>
            <option value="pop" <?php selected('genre', 'pop');?>>
                Поп
            </option>
            <option value="electronic" <?php selected('genre', 'electronic');?>>
                Электро
            </option>
            <option value="classical" <?php selected('genre', 'classical');?>>
                Классика
            </option>
            <option value="jazz" <?php selected('genre', 'jazz');?>>
                Джаз
            </option>
        </select>

        Число подписчиков
        <select required name="followers_count_operation" form="form-ad-search" class="form-control">
            <option value="all" <?php selected('followers_count_operation', "all");?>>
				Неважно
			</option>
            <option value="eq" <?php selected('followers_count_operation', "eq");?>>
				=
			</option>
            <option value="gr" <?php selected('followers_count_operation', "gr");?>>
				>
			</option>
            <option value="less" <?php selected('followers_count_operation', "less");?>>
                <
            </option>
        </select>
        <input name="followers_count" type="number" placeholder="Число подписчиков" min="0" step="1"
               class="form-control" form="form-ad-search" <?php selected_val('followers_count') ?> />

        Средний возраст подписчиков
        <select required name="followers_age_operation" form="form-ad-search" class="form-control">
            <option value="all" <?php selected('followers_age_operation', "all");?>>
				Неважно
			</option>
            <option value="eq" <?php selected('followers_age_operation', "eq");?>>
				=
			</option>
            <option value="gr" <?php selected('followers_age_operation', "gr");?>>
				>
			</option>
            <option value="less" <?php selected('followers_age_operation', "less");?>>
				<
			</option>
        </select>
        <input name="followers_age" type="number" placeholder="Средний возраст подписчиков" min="0" step="1"
               class="form-control" form="form-ad-search" <?php selected_val('followers_age') ?> />

        Средняя посещаемость
        <select required name="visits_count_operation" form="form-ad-search" class="form-control">
            <option value="all" <?php selected('visits_count_operation', "all");?>>
				Неважно
			</option>
            <option value="eq" <?php selected('visits_count_operation', "eq");?>>
				=
			</option>
            <option value="gr" <?php selected('visits_count_operation', "gr");?>>
				>
			</option>
            <option value="less" <?php selected('visits_count_operation', "less");?>>
				<
			</option>
        </select>
        <input name="visits_count" type="number" placeholder="Средняя посещаемость" min="0" step="1"
               class="form-control" form="form-ad-search" <?php selected_val('visits_count') ?> />

        Стоимость
        <select required name="price_operation" form="form-ad-search" class="form-control">
            <option value="all" <?php selected('price_operation', "all");?>>
				Неважно
			</option>
            <option value="eq" <?php selected('price_operation', "eq");?>>
				=
			</option>
            <option value="gr" <?php selected('price_operation', "gr");?>>
				>
			</option>
            <option value="less" <?php selected('price_operation', "less");?>>
				<
			</option>
        </select>
        <input name="price" type="number" placeholder="Стоимость" step="0.01" min="0"
               class="form-control" form="form-ad-search" <?php selected_val('price') ?> />

        <input type="submit" value="Найти" name="find-ad" class="btn btn-info col-sm-12" form="form-ad-search"/>

        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Соц.сеть</th>
                <th>Тип</th>
                <th>Жанр</th>
                <th>Число подписчиков</th>
                <th>Средний возраст подписчиков</th>
                <th>Средняя посещаемость</th>
                <th>Стоимость (руб.)</th>
                <th>Контакт</th>
            </tr>
            </thead>
            <tbody>
                <?php print_found_ads($link); ?>
            </tbody>
        </table>

        <form id="form-ad-search" method="post">
            <input name="blocker-ad-search" type="hidden" value="query-ad-search-<?php echo rand(0,999999); ?>">
        </form>

    </div>
    <script src="../bootstrap/js/bootstrap.js"></script>
    </body>
    </html>

<?php

function print_found_ads($link) {

    if(isset($_POST['find-ad']) && $_POST['blocker-ad-search'] != $_SESSION['current-query-ad-search']) {

        $res = mysqli_query($link, generate_query());

        if (!$res)
            printf("Errormessage: %s\n", mysqli_error($link));

        foreach ($res as $ad) {
            ?>
            <tr>
                <td><?php echo $ad['network'];?></td>
                <td><?php echo $ad['type'];?></td>
                <td><?php echo $ad['genre'];?></td>
                <td><?php echo $ad['followers_count'];?></td>
                <td><?php echo $ad['followers_age'];?></td>
                <td><?php echo $ad['visits_count'];?></td>
                <td><?php echo $ad['price'];?></td>
                <td><?php echo $ad['contact'];?></td>
            </tr>
            <?php
        }
        unset($ad);

        $_SESSION['current-query-ad-search'] = $_POST['blocker-ad-search'];
    }
}
function generate_query()
{
    if($_POST['network'] == 'all')
        $query_network = "";
    else
        $query_network = " AND ads.network = '{$_POST['network']}' ";

    if($_POST['type'] == 'all')
        $query_type = "";
    else
        $query_type = " AND ads.type = '{$_POST['type']}' ";

    if($_POST['genre'] == 'all')
        $query_genre = "";
    else
        $query_genre = " AND ads.genre = '{$_POST['genre']}' ";

    $query_followers_count = query_operation('followers_count', 'followers_count_operation');
    $query_followers_age = query_operation('followers_age', 'followers_age_operation');
    $query_visits_count = query_operation('visits_count', 'visits_count_operation');
    $query_price = query_operation('price', 'price_operation');

    $query = "SELECT *
               FROM ads 
               WHERE 
                1 = 1 
                $query_network 
                $query_type 
                $query_genre 
                $query_followers_count
                $query_followers_age
                $query_visits_count
                $query_price  
               ORDER BY price;";

    return $query;
}
function selected($name, $value) {
    if($_POST[$name] == $value) echo 'selected';
}
function selected_val($name) {
    if(!empty($_POST[$name])) echo "value = {$_POST[$name]}";
}
function query_operation($name, $name_operation) {

    if($_POST[$name_operation] == "all" || empty($_POST[$name_operation]))
        return "";
    if ($_POST[$name_operation] == "eq")
        return " AND ads.price = {$_POST[$name]} ";
    if ($_POST[$name_operation] == "less")
        return " AND ads.price < {$_POST[$name]} ";
    if ($_POST[$name_operation] == "gr")
        return " AND ads.price > {$_POST[$name]} ";

    return "";
}