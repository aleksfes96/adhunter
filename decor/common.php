<?php

include("menu.php");

function _header() {

    ?>
    <div class="header content">
        <div class="row" style="text-align: center;">
            <i class="fa fa-headphones" style="font-size:80px; color: #ac2925; margin-top:10px" aria-hidden="true"></i>
        </div>
        <div class="row" style="text-align: center;">
            <p>AD HUNTER</p>
        </div>
    </div>
    <?php
}