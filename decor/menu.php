<?php

function menu() {
    ?>
    <div class="menu btn-group">
        <?php
            main_item();
            search_item();
            manage_item();
            my_page_item();
            sign_item();
        ?>
    </div>
    <br>
    <br>
    <?php
}

function main_item() {
    ?>
    <a class='btn btn-danger' href='/adhunter'>Главная</a>
    <?php
}
function search_item() {
    ?>
    <a class='btn btn-danger' href='/adhunter/search'>Поиск</a>
    <?php
}
function my_page_item() {

    if(!isset($_SESSION['user']))
        return;

    if($_SESSION['user']['role'] == 'admin')
        $url = "/adhunter/ads";
    if($_SESSION['user']['role'] == 'advertiser')
        $url = "/adhunter/users/user.php?id={$_SESSION['user']['id']}";

    echo "<a class='btn btn-danger' href='$url'>Моя страница</a>";
}
function manage_item() {

    if(isset($_SESSION['user']) && $_SESSION['user']['role'] == 'admin') {
        ?>
        <a class='btn btn-danger' href='/adhunter/ads'>Управление</a>
        <?php
    }
}
function sign_item()
{
    if(isset($_SESSION['user'])) {
        $name = 'Выйти';
        $url = '/adhunter/auth/sign_out.php';
    }
    else {
        $name = 'Войти';
        $url = '/adhunter/auth/sign_in.php';
    }

    echo " <a class='btn btn-danger' href='$url'>$name</a>";
}