<?php

session_start();
?>
<html>
<head>
    <meta charset="utf-8">
    <title>Войти</title>
    <link href="/adhunter/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-push-4">
                <form class="forma" action="auth.php" method="post">
                    <fieldset>
                        <div class="row">
                            <h2 class="col-sm-12">Вход в систему</h2>
                            <input name="blocker" type="hidden" value="query-sign-in-<?php echo rand(0,999999);?>">
                            <div class="form-group" >
                                <input name="login" required placeholder="Логин" class="form-control">
                            </div>
                            <div class="form-group ">
                                <input type="password" name="password" required placeholder="Пароль" class="form-control ">
                            </div>
                            <input type="submit" name="sign-in" value="Войти" class="btn btn-danger col-sm-12"/>
                            <br>
                            <br>
                            <a href="sign_up.php" class="btn btn-default col-sm-12">
                                Регистрация
                            </a>
                        </div>
                    </fieldset>
                    <input name="blocker" type="hidden" value="query-sign-in-<?php echo rand(0,999999);?>" class="col-sm-12">
                </form>
            </div>
        </div>
    </div>

    <i class="fa fa-headphones" style="color: #ac2925;" aria-hidden="true"></i>

<script>

    window.mall = document.querySelector(".fa-headphones");
    freeHeight = document.documentElement.clientHeight - window.mall.offsetTop;
    freeWidth = document.documentElement.clientWidth;
    mallSize = Math.min(freeWidth,freeHeight);
    window.mall.style.fontSize = mallSize;
    window.mall.style.marginLeft = (document.documentElement.clientWidth)/2 - parseInt(window.mall.style.fontSize)/2;

</script>
<script src="/adhunter/bootstrap/js/bootstrap.js"></script>
</body>
</html>