<?php
include("../db/settings.php");
include("common.php");

session_start();
?>
<html>
<head>
    <meta charset="utf-8">
    <link href="/adhunter/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>

<body>
<div class="container" style="margin-top: 10%;">

<?php

    $link = db_connection();

    if(!isset($_SESSION['current-query-sign-up']))
        $_SESSION['current-query-sign-up'] = "null";

    if(isset($_POST['sign-up']) && $_POST['blocker'] != $_SESSION['current-query-sign-up']) {

        $_SESSION['current-query-sign-up'] = $_POST['blocker'];

        if(!login_is_free($link, $_POST['login']) || $_POST['login'] == 'admin') {
            ?>
                <div class="row">
                    <div class="alert alert-danger col-sm-4 col-sm-push-4" style="text-align: center;" role="alert">
                        Введенный Вами логин уже занят.
                    </div>
                </div>
                <div class="row">
                    <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-danger col-sm-push-5 col-sm-2"/>
                        Вернуться назад
                    </a>
                </div>
            <?php
            exit;
        }

        $id = create_id($link, 'users');
        $hash = get_hash($_POST['password']);
        $query = "INSERT INTO users (id, name, role, login, hash) 
                  VALUES (
                  $id,
                  '{$_POST['name']}',
                  'advertiser',
                  '{$_POST['login']}',
                  '$hash');";


        if (mysqli_query($link, $query)) {

            $_SESSION['user'] = array('id' => $id, 'role' => 'advertiser');
            ?>
                <div class="row">
                    <div class="alert alert-info col-sm-4 col-sm-push-4" style="text-align: center;" role="alert">
                        Вы успешно зарегистрированы.<br>Можете приступать
                        к размещению объявлений.<br>Продуктивного дня!
                    </div>
                </div>
                <div class="row">
                    <a href="/adhunter/users/user.php?id=<?php echo $id;?>" class="btn btn-danger col-sm-push-5 col-sm-2"/>
                        Начать
                    </a>
                </div>
            <?php
        }
        else {
            ?>
                <div class="row">
                    <div class="alert alert-danger col-sm-4 col-sm-push-4" style="text-align: center;" role="alert">
                        Ошибка сервера:
                        <?php printf("Errormessage: %s\n", mysqli_error($link)); ?>
                    </div>
                </div>
                <div class="row">
                    <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-danger col-sm-push-5 col-sm-2"/>
                    Вернуться назад
                    </a>
                </div>
            <?php
        }
    }
    else {

        header("Location: {$_SERVER['HTTP_REFERER']}");
        exit;
    }
?>
</div>
<script src="/adhunter/bootstrap/js/bootstrap.js"></script>
</body>
</html>

