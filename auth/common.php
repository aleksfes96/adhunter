<?php

function get_hash($data) {

    $salt='default_salt_@#$%@#$%^U';

    if (isset($config['salt']))
        $salt=$config['salt'];

    return hash('sha256',$data.$salt);
}
function is_admin() {
    return isset($_SESSION['user']) && $_SESSION['user']['role'] == 'admin';
}
function is_guest() {
    return !isset($_SESSION['user']);
}
function login_is_free($link, $login) {

    $res = mysqli_query($link, "SELECT * FROM users WHERE login = '$login';");
    return mysqli_num_rows($res) == 0;
}
function create_id($link, $table_name) {

    do {
        $id = rand(1,1000);
        $res = mysqli_query($link,"SELECT id FROM $table_name WHERE id=$id;");

    } while(mysqli_num_rows($res));

    return $id;
}