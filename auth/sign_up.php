<?php
include("../db/settings.php");
include("../decor/common.php");

session_start();
?>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <title>Регистрация</title>
    <link href="/adhunter/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/adhunter/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Trirong:300i" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/40c1954f63.js"></script>
</head>

<body>
<?php

    $link = db_connection();
    _header();
    menu();
?>
<div class="content">

    <div class="row">
        <div class="col-sm-4 col-sm-push-4">
            <form class="formSignUp" action="registration.php" method="post">
                <fieldset>
                    <div class="row">
                        <legend class="col-sm-12">
                            Регистрация
                        </legend>
                        <input name="blocker" type="hidden" value="query-sign-up-<?php echo rand(0,999999);?>">
                        <div class="form-group ">
                            <label>Введите имя </label>
                            <input name="name" required maxlength="100"
                                   placeholder="Иван" class="form-control">
                        </div>
                        <div class="form-group ">
                            <label>Ваш логин</label>
                            <input name="login" required placeholder="ivan" maxlength="15" class="form-control">
                        </div>
                        <div class="form-group" >
                            <label>Ваш пароль</label>
                            <input name="password" required type="password" placeholder="Пароль" maxlength="15" class="form-control">
                        </div>
                        <input type="submit" name="sign-up" value="Стать рекламодателем" class="btn btn-danger col-sm-12"/>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>
<script src="/adhunter/bootstrap/js/bootstrap.js"></script>
</body>
</html>
