<?php
include("../db/settings.php");
include("common.php");

    session_start();
?>
<html>
<head>
    <meta charset="utf-8">
    <link href="/adhunter/bootstrap/css/bootstrap.css" rel="stylesheet">
</head>

<body>
<?php

    $link = db_connection();

    if(isset($_POST['sign-in'], $_POST['login'], $_POST['password'])
        && $_POST['blocker'] != $_SESSION['current-query-sign-in']) {

        $_SESSION['current-query-sign-in'] = $_POST['blocker'];

        $hash = get_hash($_POST['password']);
        $res = mysqli_query($link,
            "SELECT id, role 
                    FROM users 
                    WHERE login = '{$_POST['login']}' AND hash = '$hash';"
        );

        $user = mysqli_fetch_array($res);

        if(!empty($user)) {

            $_SESSION['user'] = array('id' => "{$user['id']}", 'role' => "{$user['role']}");
            if($user['role'] == 'admin')
                $url = '/adhunter/ads/';
            if($user['role'] == 'advertiser')
                $url = "/adhunter/users/user.php?id={$user['id']}";

            header("location:{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}{$url}");
            exit;
        }
        else {
            ?>
            <div class="container" style="margin-top: 10%;">
                <div class="row">
                    <div class="alert alert-danger col-sm-4 col-sm-push-4" style="text-align: center;" role="alert">
                        Неверный логин или пароль
                    </div>
                </div>
                <div class="row">
                    <a href="<?php echo $_SERVER['HTTP_REFERER'];?>" class="btn btn-danger col-sm-push-5 col-sm-2"/>
                    Вернуться назад
                    </a>
                </div>
            </div>
            <?php
        }
    }
?>
<script src="/adhunter/bootstrap/js/bootstrap.js"></script>
</body>
</html>
